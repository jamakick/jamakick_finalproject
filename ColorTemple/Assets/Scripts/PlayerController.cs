﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {

    public float maxSpeed = 5f;
    bool facingRight = true;

    Animator anim;

    bool grounded = false;
    public Transform groundCheck;
    float groundRadius = 0.2f;
    public LayerMask whatIsGround;
    public float jumpForce = 550f;

    private Rigidbody2D rb2d;

    public leverScript closeLever;
    public MineralScript pickUpGold;
    public PlaceGoldScript placeGold;
    public SpriteRenderer mineralHolder;
    public Text playerText;

    public bool ladder;
    public float climbSpeed;
    private float climbVel;
    private float gravity;

	// Use this for initialization
	void Start ()
    {
        mineralHolder.sprite = null;
        anim = GetComponent<Animator>();
        rb2d = GetComponent<Rigidbody2D>();
        closeLever = null;
        pickUpGold = null;
        placeGold = null;
        MineralScript.hasGold = false;

        gravity = rb2d.gravityScale;

        StartCoroutine(waitForText());
	}

    IEnumerator waitForText()
    {
        yield return new WaitForSeconds(2.0f);

        playerText.text = "What happened to me?";

        yield return new WaitForSeconds(3.0f);

        playerText.text = "I need to find my way out";

        yield return new WaitForSeconds(3.0f);

        playerText.text = "Press left and right to move, space to jump, and F to interact";

        yield return new WaitForSeconds(4.0f);

        playerText.text = "";
    }
	
	// Update is called once per frame
	void FixedUpdate ()
    {

        grounded = Physics2D.OverlapCircle(groundCheck.position, groundRadius, whatIsGround);

        anim.SetBool("ground", grounded);

        anim.SetFloat("vSpeed", rb2d.velocity.y);



        float move = Input.GetAxis("Horizontal");

        anim.SetFloat("speed", Mathf.Abs(move));

        rb2d.velocity = new Vector2(move * maxSpeed, rb2d.velocity.y);

        if(move > 0 && !facingRight)
        {
            Flip();
        }
        else if(move < 0 && facingRight)
        {
            Flip();
        }

		
	}

    void Update()
    {
        if (grounded && Input.GetButtonDown("Jump"))
        {
            anim.SetBool("ground", false);
            rb2d.AddForce(new Vector2(0, jumpForce));
        }

        if (Input.GetKeyDown(KeyCode.F))
        {
            if (closeLever != null)
            {
                closeLever.SwitchAction();
            }

            if (pickUpGold != null)
            {
                pickUpGold.pickUpMineral();
            }

			if (placeGold != null && MineralScript.hasGold == true)
            {
                placeGold.place();
            }
        }

        if(ladder)
        {
            rb2d.gravityScale = 0f;
            climbVel = climbSpeed * Input.GetAxisRaw("Vertical");

            rb2d.velocity = new Vector2(rb2d.velocity.x, climbVel);
        }

        if(!ladder)
        {
            rb2d.gravityScale = gravity;
        }
    }

    void Flip ()
    {
        facingRight = !facingRight;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }
}
