﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MineralScript : MonoBehaviour {

    public static bool hasGold;

    public Sprite goldSprite;
    public SpriteRenderer mineralHolder;

    public Text playerText;

    IEnumerator mineralText()
    {
        yield return new WaitForSeconds(1.0f);

        playerText.text = "I should be able to do something with this.";

        yield return new WaitForSeconds(2.0f);

        playerText.text = "";
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            other.GetComponent<PlayerController>().pickUpGold = this;
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            if (other.GetComponent<PlayerController>().pickUpGold == this)
            {
                other.GetComponent<PlayerController>().pickUpGold = null;
            }
        }
    }

    public void pickUpMineral()
    {
        mineralHolder.sprite = goldSprite;
        hasGold = true;

        StartCoroutine(mineralText());

        gameObject.GetComponent<SpriteRenderer>().enabled = false;
    }
}
