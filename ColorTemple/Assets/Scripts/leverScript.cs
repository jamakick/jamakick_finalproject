﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class leverScript : MonoBehaviour {

    public GameObject goldLock;

    public GameObject trapLever;
    public GameObject goldLever;

    public GameObject trapdoor;

    public Text playerText;

    private bool trapLeverPressed = false;
    private bool goldLeverPressed = false;

    IEnumerator leverText()
    {
        yield return new WaitForSeconds(1.0f);

        playerText.text = "It sounds like the lever moved something.";

        yield return new WaitForSeconds(2.0f);

        playerText.text = "";
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            other.GetComponent<PlayerController>().closeLever = this;
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            if (other.GetComponent<PlayerController>().closeLever == this)
            {
                other.GetComponent<PlayerController>().closeLever = null;
            }
        }
    }

    public void SwitchAction()
    {
        if (trapLeverPressed != true)
        {
            trapLever.transform.Rotate(new Vector3(0, 0, -60));
            trapLeverPressed = true;

            trapdoor.transform.eulerAngles = new Vector3(0, 0, 70);

            trapdoor.transform.position = new Vector3(13.1f, -13.22f, 0);

            StartCoroutine(leverText());
        }

        else if (goldLeverPressed != true)
        {
            goldLever.transform.Rotate(new Vector3(0, 0, -60));
            goldLeverPressed = true;

            GameObject.Destroy(goldLock);

            StartCoroutine(leverText());
        }
    }
}
