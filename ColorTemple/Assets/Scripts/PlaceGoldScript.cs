﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlaceGoldScript : MonoBehaviour {

    public SpriteRenderer mineralHolder;
    public SpriteRenderer goldNugPlaced;
    public Sprite goldSprite;
    public GameObject areaLock;
    public GameObject goldHolder;
    public Text playerText;

    IEnumerator doorText()
    {
        yield return new WaitForSeconds(1.0f);

        playerText.text = "The way is clear! I can make my way up!";

        yield return new WaitForSeconds(2.0f);

        playerText.text = "";
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            other.GetComponent<PlayerController>().placeGold = this;
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            if (other.GetComponent<PlayerController>().placeGold == this)
            {
                other.GetComponent<PlayerController>().placeGold = null;
            }
        }
    }

    public void place()
    {
        mineralHolder.sprite = null;
        goldNugPlaced.sprite = goldSprite;

        Color goldNew = goldNugPlaced.color;
        goldNew.a = 0.2f;
        goldNugPlaced.color = goldNew;

        Color areaLockNew = areaLock.GetComponent<SpriteRenderer>().color;
        areaLockNew.a = 0.1f;
        areaLock.GetComponent<SpriteRenderer>().color = areaLockNew;

        areaLock.GetComponent<BoxCollider2D>().enabled = false;

        StartCoroutine(doorText());

        goldHolder.GetComponent<MeshRenderer>().enabled = false;
    }


}
