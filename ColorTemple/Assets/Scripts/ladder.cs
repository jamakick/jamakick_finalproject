﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ladder : MonoBehaviour {

    private PlayerController player;

	// Use this for initialization
	void Start () {
        player = FindObjectOfType<PlayerController>();
		
	}
	
    void OnTriggerEnter2D (Collider2D other)
    {
        if(other.name == "Player")
        {
            player.ladder = true;
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.name == "Player")
        {
            player.ladder = false;
        }
    }
}
