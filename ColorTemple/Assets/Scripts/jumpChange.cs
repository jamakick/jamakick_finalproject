﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class jumpChange : MonoBehaviour {

    public Text playerText;

    IEnumerator ladderText()
    {

        playerText.text = "Press the up arrow to climb ladders";

        yield return new WaitForSeconds(2.0f);

        playerText.text = "";
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            other.GetComponent<PlayerController>().jumpForce = 400f;
            StartCoroutine(ladderText());
            
        }
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
